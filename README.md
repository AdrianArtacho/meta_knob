This patch is part of the [_META_ Environment](https://bitbucket.org/AdrianArtacho/meta4live/src/master/).

![META_logo](https://bitbucket.org/AdrianArtacho/meta4live/raw/HEAD/META_logo.png)

# META_knob

This M4L device receives midi CC messages (by default CC 22-25 and 105-107). The eight knobs can be mapped to control any UI object within Live:

1. Click on one of the macro buttons above the  knob (e.g. `macro1`). The green `stay` button will light up *green* and change to `on`.

2. Click on the UI object you wish to control with that knob (e.g. a track fx knob). Be careful not to click on any other control but the on e you wish to map to the knob.

3. After a few seconds, the `on` toggle will return to normal (`stay`). The mapping is complete.

4. Enjoy!

Now, if you send CC values (e.g. `CC22,51`) to the device, the corresponding knob will. make the mapped UI object to change. **Note:** The CC toggle needs to be on (blue) to allow CC input.

![Repo:META:knobhub](https://docs.google.com/drawings/d/e/2PACX-1vTXo8TWtOOub2kkweOR7szsLz_5xbdaPtdW6AMKLQssTwX_AdCdEUASl5uSnVukpGoRJIpY7HJ8tfro/pub?w=423&h=276)

### Controls and features

**[ memory ]**. Self explanatory once you click on it.

**[ ctrl ].** Shows the values stored with the device (and the set). Check that these save the settings from one session to another.

**[ CC ].** Allows to choose between *ON* (takes midi CC in) or *OFF* (optimal for a control surface such as the APCKey25 to take over).

**[ random ].** Generate random knob values.

**[ generate ].** Cenerate a continuous stream af CC so that you can store the CC settings on a clip (you need to record it).

**[ 6 small toggles ].** You can decide to process selectively some of the CC inouts using those knobs.

**[ nonzero ].** If on, the device ignore zero values, scaling the input accordingly.

**[ readme ].** Link to online repository and README file.

_____

## Exporting Max For Live Device

When exporting as a M4L device, remember to do once for midi tracks (**META_knob-M**) and again for audio tracks (**META_knob-A**)

### CC midi input for macro controls

[CC messages reference](https://docs.google.com/spreadsheets/d/e/2PACX-1vSNPI08rSjfvctjYF4SNNbjUE4hrDPGBhdvDISSuLHciy4LdjDJawKAYrcGManydX4azAuSUnZnFZbb/pubhtml?gid=0&single=true)

### Controlling behavior via Clips names

You can control the behaviour with clips, for example:

PITCH *[k5 m1]

‘PITCH’ is just a name to know what the knob does

K5 → knob 5 from the APCkey25

M1 → macro 1 from a given rack

A knob could control multiple macros:

[k5 m1 m3 m7]

The asterisk before the [...] resets that knob to control ONLY what is described in this clip, forgetting other mappings previously established.

### META_knob

There’s a flavout for midi tracks (META_knob-M, and another one for audio tracks META_knob-A)

You can control the behaviour with clips, for example:

PITCH 

  `*[k5 m1]`

‘PITCH’ is just a name to know what the knob does

K5 → knob 5 from the APCkey25
M1 → macro 1 from a given rack

A knob could control multiple macros:

`[k5 m1 m3 m7]`

The asterisk before the *...* resets that knob to control ONLY what is described in this clip, forgetting other mappings previously established.

### Credits

Reference...

---

# To-Do

- For some reason, the values stored in `[pattrstorage]` object cannot be edited (and saved with the set). For now, it is just hard-wired to 22-23-24-25-104-105-106-107. Why?
- HUGE BUG: the ranges / units of the knobs get lost in the process of mapping, and when I restart, i need to do the mapping AGAIN EVERY TIME! not trustworthy for production
- <> on starting the device [live.thisdevice] read from the [pattr] objects which are the id's that have been stored from last time. Then write it to the [coll ---] object. This solves the problem of erasing all older mappings every time I compile (export as m4l device)a new version of the device.
- Should I go back to the 3-click mapping method? there is a problem when I try to map something on a different track... maybe I can have mappable button as the 'third' click... which I could control with an external controller...
- document META_knob in repo
- Think: only two knobs can be operated at the same time; there are some good ideas about macro positioning that might be worth kept (filter always here... most expressive knob after filter here... so that I can use my two hands in a smart way)
- add LFO's? to some controllers? how?
- Add "hidden macros" so that I can set parameters via clips? or maybe use clips with automations on the device track for those, more specific tinkering parameters? ...another idea would be, as a rule of thumb, to use actively only the top 4 macros... (DUMB!)
- Have the togles in [META_knob] by default ON!!
- Saves preset with the sets? Test that
- DEBUG Save as file, load as file
- MEMORY SETTINGS >> stored settings tests
- why did knobs 3 and 7 keep the stored ids, but the others didn't?
- CC on by default on OPEN! Test that the settings get stored with the patch (put several objects in a track, save, reopen)
- {} document the "generate/capture" process to store Macro positions as clips.
- DEBUG save/load presets doesn't work!!

---

##### Error log

live.observer: property cannot be listened to
live.observer: property cannot be listened to
live.observer: property cannot be listened to
live.observer: property cannot be listened to
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: get: no valid object set
live.object: get: no valid object set
live.object: get: no valid object set
live.observer: property cannot be listened to
live.observer: property cannot be listened to
live.observer: property cannot be listened to
live.observer: property cannot be listened to
live.observer: property cannot be listened to
live.observer: property cannot be listened to
live.observer: property cannot be listened to
live.observer: property cannot be listened to

live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.object: Live API is not initialized, use live.thisdevice to determine when initialization is complete
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
live.observer: set id: invalid id
