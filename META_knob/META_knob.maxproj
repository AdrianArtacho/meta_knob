{
	"name" : "META_knob",
	"version" : 1,
	"creationdate" : 3711991325,
	"modificationdate" : 3748946166,
	"viewrect" : [ 23.0, 103.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"META_knob.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"3StateToggle.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"singleton" : 				{
					"bootpath" : "~/Documents/Max 8/Projects/[META]/META4Live/meta_knob/gui_toggle",
					"projectrelativepath" : "../gui_toggle"
				}

			}
,
			"KnobSettings.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"ParameterRange.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"live.property.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"macroButton.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 1835887981,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
